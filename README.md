# Platformer

A simple platformer made in C++ for my second term university project.

## Licence

All code is licensed under the [GNU GPLv3](https://www.gnu.org/licenses/gpl.html) unless otherwise specified.

All other non-code assets (images etc.) are licenced under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) unless otherwise specified.

[Open Sans Bold](OpenSans-Bold.ttf) is licensed under the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).

## Dependencies

Platformer uses the following dependencies for development:

- [cmake](https://cmake.org/)
- [make](https://www.gnu.org/software/make/)

The code itself depends on:

- [SDL2](https://www.libsdl.org/)
	- SDL\_Image
	- SDL\_TTF
- [boost](http://www.boost.org/)
	- boost\_filesystem

## Building

To build the program, run the following commands:

    cmake .
    make

The binary will be outputted to the bin/ folder, which will be created if it doesn't already exist.

To run the program, run the following command:

    ./bin/platformer
