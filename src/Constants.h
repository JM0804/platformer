/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#pragma once

// Level file block type representations
const char BLOCKTYPE_NULL			        = ' ';
const char BLOCKTYPE_SPAWNPOINT             = 'S';
const char BLOCKTYPE_BLOCK_DIRT		        = 'D';
const char BLOCKTYPE_BLOCK_GRASS	        = 'G';
const char BLOCKTYPE_ENEMY_DINO		        = 'E';
const char BLOCKTYPE_ENEMY_DINO_ON_GRASS    = 'e';
const char BLOCKTYPE_ENEMY_FLYER            = '2';
const char BLOCKTYPE_SPAWNPOINT_ON_GRASS    = 'g';
const char BLOCKTYPE_BLOCK_LADDER           = 'L';
const char BLOCKTYPE_BLOCK_LADDER_ON_DIRT   = 'l';
const char BLOCKTYPE_EXITPOINT              = 'X';
const char BLOCKTYPE_ENEMYCOLLIDER          = 'C';
const char BLOCKTYPE_ENEMYCOLLIDER_ON_GRASS = 'c';

// Global variables
const int BLOCK_SIZE_MIN = 30;
const int BLOCK_SIZE_MAX = 70;
const float BLOCK_SIZE = 96;
const int WINDOW_WIDTH = 1024;
const int WINDOW_HEIGHT = 768;
const int angle[4] = {0, 90, 180, 270};

// Z-indexes
const int BLOCK_Z_MIN = 1;
const int BLOCK_Z_DIRT = 1;
const int BLOCK_Z_LADDER = 10;
const int BLOCK_Z_PLAYER = 20;
const int BLOCK_Z_ENEMY = 30;
const int BLOCK_Z_EXITPOINT = 40;
const int BLOCK_Z_GRASS = 50;
const int BLOCK_Z_MAX = 50;

// Sprite textures
SDL_Texture* IMAGE_DIRT_1 = NULL;
SDL_Texture* IMAGE_DIRT_2 = NULL;
SDL_Texture* IMAGE_DIRT_3 = NULL;
SDL_Texture* IMAGE_GRASS_1 = NULL;
SDL_Texture* IMAGE_GRASS_2 = NULL;
SDL_Texture* IMAGE_GRASS_3 = NULL;
SDL_Texture* IMAGE_DINO = NULL;
SDL_Texture* IMAGE_FLYER = NULL;
SDL_Texture* IMAGE_PLAYER = NULL;
SDL_Texture* IMAGE_LADDER = NULL;
SDL_Texture* IMAGE_LADDER_ON_DIRT = NULL;
SDL_Texture* IMAGE_EXITPOINT = NULL;

// Window and renderer
SDL_Renderer* renderer = NULL;
SDL_Window* window = NULL;

// Font colours
const SDL_Color colourWhite = {0xFF, 0xFF, 0xFF};

// Offsets
float centreOffsetX = 0;
float centreOffsetY = 0;
float cameraOffsetX = 0;
float cameraOffsetY = 0;

// Level height and width
int levelHeight = 0;
int levelWidth = 0;

// Player spawn point
int spawnX = 0;
int spawnY = 0;
