/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#pragma once

using namespace std;

class Sprite {
    public:
        Sprite(float xPos, float yPos);
        virtual void update();
        virtual bool collidedWith(Sprite* otherSprite);
        bool collidedWithAny(vector<auto>& spriteList);
        
        // Getters
        virtual SDL_Rect* getRect();
        virtual float getPosX();
        virtual float getPosY();
        virtual int getPosZ();
        virtual float getHeight();
        virtual float getWidth();
        virtual float getVelocityX();
        virtual float getVelocityY();
        virtual float getCentreX();
        virtual float getCentreY();
        virtual float getBottomY();
        virtual float getRightX();
        virtual SDL_Texture* getImage();
        virtual int getAngle();
        virtual SDL_RendererFlip getFlip();
        
        // Setters
        virtual void setRect(SDL_Rect newRect);
        virtual void setPos(float newX, float newY);
        virtual void setPosX(float newX);
        virtual void setPosY(float newY);
        virtual void setPosZ(int newZ);
        virtual void setHeight(float newHeight);
        virtual void setWidth(float newWidth);
        virtual void setVelocityX(float newVelocityX);
        virtual void setVelocityY(float newVelocityY);
        virtual void setImage(SDL_Texture* newImage);
        virtual void setAngle(int newAngle);
        virtual void setFlip(SDL_RendererFlip newFlip);
        
        // Modders
        virtual void modVelocityX(float xMod);
        virtual void modVelocityY(float yMod);
        virtual void modPosX(float xMod);
        virtual void modPosY(float yMod);
    protected:
        SDL_Texture* image;
        SDL_Rect rect;
        int angle = 0;
        SDL_RendererFlip flip;
        float x = 0;
        float y = 0;
        float h = 0;
        float w = 0;
        int z;
        static const int VELOCITY = 0.03;
        float velocityX = 0;
        float velocityY = 0;
};

Sprite::Sprite(float xPos, float yPos) {
    
}

void Sprite::update() {
    SDL_Rect rect = {
        (int)(x + centreOffsetX + cameraOffsetX),
        (int)(y + centreOffsetY + cameraOffsetY),
        (int)w,
        (int)h
    };
    
    SDL_RenderCopyEx(renderer, image, NULL, &rect, angle, NULL, flip);
}

bool Sprite::collidedWith(Sprite* otherSprite) {
    return SDL_HasIntersection(getRect(), otherSprite->getRect());
}

bool Sprite::collidedWithAny(vector<auto>& spriteList) {
    for (auto &check : spriteList) {
        if (collidedWith(&check)) {
            return true;
        }
    }
    return false;
}

// Getters
SDL_Rect* Sprite::getRect() {
    rect = {
        (int)x,
        (int)y,
        (int)w,
        (int)h
    };
    return &rect;
}

float Sprite::getPosX() {
    return x;
}

float Sprite::getPosY() {
    return y;
}

int Sprite::getPosZ() {
    return z;
}

float Sprite::getHeight() {
    return h;
}

float Sprite::getWidth() {
    return w;
}

float Sprite::getVelocityX() {
    return velocityX;
}

float Sprite::getVelocityY() {
    return velocityY;
}

float Sprite::getCentreX() {
    return x + w/2;
}

float Sprite::getCentreY() {
    return y + h/2;
}

float Sprite::getBottomY() {
    return y+h;
}

float Sprite::getRightX() {
    return x+w;
}

SDL_Texture* Sprite::getImage() {
    return image;
}

int Sprite::getAngle() {
    return angle;
}

SDL_RendererFlip Sprite::getFlip() {
    return flip;
}

// Setters
void Sprite::setRect(SDL_Rect newRect) {
    x = newRect.x;
    y = newRect.y;
    w = newRect.w;
    h = newRect.h;
}

void Sprite::setPos(float newX, float newY) {
    x = newX;
    y = newY;
}

void Sprite::setPosX(float newX) {
    x = newX;
}

void Sprite::setPosY(float newY) {
    y = newY;
}

void Sprite::setPosZ(int newZ) {
    z = newZ;
}

void Sprite::setHeight(float newH) {
    h = newH;
}

void Sprite::setWidth(float newW) {
    w = newW;
}

void Sprite::setVelocityX(float newVelocityX) {
    velocityX = newVelocityX;
}

void Sprite::setVelocityY(float newVelocityY) {
    velocityY = newVelocityY;
}

void Sprite::setImage(SDL_Texture* newImage) {
    image = newImage;
}

void Sprite::setAngle(int newAngle) {
    angle = newAngle;
}

void Sprite::setFlip(SDL_RendererFlip newFlip) {
    flip = newFlip;
}

// Modders
void Sprite::modVelocityX(float xMod) {
    velocityX += xMod;
}

void Sprite::modVelocityY(float yMod) {
    velocityY += yMod;
}

void Sprite::modPosX(float xMod) {
    x += xMod;
}

void Sprite::modPosY(float yMod) {
    y += yMod;
}
