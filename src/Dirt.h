/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "Sprite.h"
#pragma once

class Dirt : public Sprite {
    public:
        Dirt(float xPos, float yPos);
};

Dirt::Dirt(float xPos, float yPos) : Sprite(xPos, yPos) {
    w = BLOCK_SIZE;
    h = BLOCK_SIZE;
    x = xPos*w;
    y = yPos*h;
    z = BLOCK_Z_DIRT;
    int texture = rand() % 3;
    switch (texture) {
        case 0:
            image = IMAGE_DIRT_1;
            break;
        case 1:
            image = IMAGE_DIRT_2;
            break;
        case 2:
            image = IMAGE_DIRT_3;
            break;
    }
    int chooseAngle[4] = {0, 90, 180, 270};
    angle = chooseAngle[rand() % 4];
    int chooseFlip = rand() % 3;
    switch (chooseFlip) {
        case 0:
            flip = SDL_FLIP_NONE;
            break;
        case 1:
            flip = SDL_FLIP_HORIZONTAL;
            break;
        case 2:
            flip = SDL_FLIP_VERTICAL;
            break;
    }
}
