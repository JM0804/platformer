/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "Sprite.h"
#pragma once

class Grass : public Sprite {
    public:
        Grass(float xPos, float yPos);
};

Grass::Grass(float xPos, float yPos) : Sprite(xPos, yPos) {
    w = BLOCK_SIZE;
    h = BLOCK_SIZE/2;
    x = xPos*BLOCK_SIZE;
    y = (yPos*BLOCK_SIZE)+h;
    z = BLOCK_Z_GRASS;
    int texture = rand() % 2;
    switch (texture) {
        case 0:
            image = IMAGE_GRASS_1;
            break;
        case 1:
            image = IMAGE_GRASS_2;
            break;
        case 2:
            Grass::image = IMAGE_GRASS_3;
            break;
    }
    int chooseFlip = rand() % 2;
    switch (chooseFlip) {
        case 0:
            flip = SDL_FLIP_NONE;
            break;
        case 1:
            flip = SDL_FLIP_HORIZONTAL;
            break;
    }
}
