/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "Sprite.h"
#pragma once

class EnemyCollider : public Sprite {
    public:
        EnemyCollider(float xPos, float yPos);
        void update();
};

EnemyCollider::EnemyCollider(float xPos, float yPos) : Sprite(xPos, yPos) {
    image = NULL;
    w = BLOCK_SIZE;
    x = xPos*w;
    h = BLOCK_SIZE;
    y = yPos*h;
    z = -1;
    angle = 0;
    flip = SDL_FLIP_NONE;
}

void EnemyCollider::update() {
    SDL_Rect rect = {
        (int)(x + centreOffsetX + cameraOffsetX),
        (int)(y + centreOffsetY + cameraOffsetY),
        (int)w,
        (int)h
    };
    
    SDL_RenderCopyEx(renderer, image, NULL, &rect, 0, NULL, flip);
}
