/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <boost/filesystem.hpp>
#include "Constants.h"
#include "Dirt.h"
#include "Grass.h"
#include "Player.h"
#include "Ladder.h"
#include "ExitPoint.h"
#include "EnemyDino.h"
#include "EnemyCollider.h"

using namespace std;
using namespace boost::filesystem;

// End the game when the player has won or lost
void endGame();

// Initialise SDL and associated libraries
bool init();

// Close SDL and associated libraries
void close();

// Display text in the centre of the window
int displayCentreText(string text);

// Load all texture, map and data files
bool loadAllFiles();

// Load a level file and generate sprites
bool loadLevel(int number);

// Load an image file into a texture
SDL_Texture* loadTexture(string path);


TTF_Font* OpenSansBold = NULL;

vector<Dirt> dirtList;
vector<Grass> grassList;
vector<EnemyDino> enemyDinoList;
vector<Ladder> ladderList;
vector<ExitPoint> exitPointList;
vector<EnemyCollider> enemyColliderList;

Player player = Player(0, 0);


void endGame(bool won) {
	if (won) {
		displayCentreText("You won!");
	}
	else {
		displayCentreText("You lost!");
	}
	SDL_Delay(2000);
	close();
}


bool init() {
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        printf("Unable to initialise SDL: %s\n", SDL_GetError());
        return false;
    }
    else {
		printf("SDL initialised\n");
	}
    
    int flags = IMG_INIT_PNG;
    if ((IMG_Init(flags) & flags) != flags) {
		printf("Unable to initialise SDL_Image: %s\n", IMG_GetError());
		return false;
	}
	else {
		printf("SDL_Image initialised\n");
	}
    
    if (TTF_Init() == -1) {
        printf("Unable to initialise SDL_TTF: %s\n", TTF_GetError());
        return false;
    }
    else {
        printf("SDL_TTF initialised\n");
    }
	
	window = SDL_CreateWindow("Platformer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE|SDL_WINDOW_MAXIMIZED);
    if (window == NULL) {
		printf("Unable to create window: %s\n", SDL_GetError());
		return false;
	}
	else {
		printf("Window created\n");
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
		if (renderer == NULL) {
			printf("Unable to create renderer: %s\n", SDL_GetError());
			return false;
		}
	}
    return true;
}


void close() {
	IMG_Quit();
	SDL_Quit();
}


int displayCentreText(string text) {
    // Get the height and width of the window
    int windowHeight, windowWidth;
    SDL_GetWindowSize(window, &windowWidth, &windowHeight);
    
    // Create string stream and put text into it
    stringstream textStream;
    textStream.str(text);
    
    // Create rect and resize it to fit the text
    SDL_Rect rect;
    TTF_SizeText(OpenSansBold, textStream.str().c_str(), &rect.w, &rect.h);
    
    // Centre the rect in the window
    rect.x = windowWidth/2 - rect.w/2;
    rect.y = windowHeight/2 - rect.h/2;
    
    // Create the text surface and convert it to a texture for faster rendering
    SDL_Surface* innerText = TTF_RenderText_Blended(OpenSansBold, textStream.str().c_str(), colourWhite);
    SDL_Texture* innerTextTexture = SDL_CreateTextureFromSurface(renderer, innerText);
    
    // Render the text and destroy its texture
    SDL_RenderCopy(renderer, innerTextTexture, NULL, &rect);
    SDL_DestroyTexture(innerTextTexture);
    
	return 0;
}


bool loadAllFiles() {
    IMAGE_DIRT_1 = loadTexture("textures/dirt1.png");
    IMAGE_DIRT_2 = loadTexture("textures/dirt2.png");
    IMAGE_DIRT_3 = loadTexture("textures/dirt3.png");
    IMAGE_GRASS_1 = loadTexture("textures/grass1.png");
    IMAGE_GRASS_2 = loadTexture("textures/grass2.png");
    IMAGE_DINO = loadTexture("textures/enemydino.png");
    IMAGE_FLYER = NULL;
    IMAGE_PLAYER = loadTexture("textures/player.png");
    IMAGE_LADDER = loadTexture("textures/ladder.png");
    IMAGE_LADDER_ON_DIRT = loadTexture("textures/ladderondirt.png");
    IMAGE_EXITPOINT = loadTexture("textures/exitpoint.png");
    OpenSansBold = TTF_OpenFont("OpenSans-Bold.ttf", 150);
	return true;
}

SDL_Texture* loadTexture(string path) {
    SDL_Texture* newTexture = NULL;

    // Load image into surface
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if (loadedSurface == NULL) {
        printf("Unable to load image %s! SDL_Image Error: %s\n", path.c_str(), IMG_GetError());
    }
    else {
        // Make white pixels transparent
        SDL_SetColorKey(loadedSurface, 1, SDL_MapRGB(loadedSurface->format, 255, 255, 255));
        // Convert surface to texture
        newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
        if (newTexture == NULL) {
            printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        }
        // Free unused surface
        SDL_FreeSurface(loadedSurface);
    }
    return newTexture;
}


bool loadLevel(int number) {
    // Display loading text
    displayCentreText("Loading...");
    
    // Empty sprite lists from last level
    dirtList.clear();
    grassList.clear();
    enemyDinoList.clear();
    ladderList.clear();
    exitPointList.clear();
    enemyColliderList.clear();
    
    // Load level file into lines vector, and count number of rows and columns
    ifstream levelFile("levels/" + to_string(number) + ".lvl");
    string line;
    vector<string> lines;
    
    while (levelFile.good()) {
        getline(levelFile, line);
        if (line != "") {
            lines.push_back(line);
        }
        
    }
    
    int yBlocks = lines.size();
    int xBlocks = lines[0].size();
    
    levelHeight = yBlocks*BLOCK_SIZE;
    levelWidth = xBlocks*BLOCK_SIZE;
    
    // Read each character in turn and create the associated sprite
    char key;
    for (int x = 0; x < xBlocks; x++) {
        for (int y = 0; y < yBlocks; y++) {
            key = lines[y][x];
            //key = BLOCKTYPE_BLOCK_DIRT; // Testing: forces character (overrides level file)
            
            switch (key) {
                case BLOCKTYPE_BLOCK_DIRT: {
                    Dirt dirt = Dirt(x, y);
                    dirtList.push_back(dirt);
                    break;
                }
                case BLOCKTYPE_BLOCK_GRASS: {
                    Grass grass = Grass(x, y);
                    grassList.push_back(grass);
                    break;
                }
                case BLOCKTYPE_SPAWNPOINT_ON_GRASS: {
                    Grass grass = Grass(x, y);
                    grassList.push_back(grass);
                    player = Player(x, y);
                    spawnX = x*BLOCK_SIZE;
                    spawnY = y*BLOCK_SIZE;
                    break;
                }
                case BLOCKTYPE_BLOCK_LADDER: {
                    Ladder ladder = Ladder(x, y);
                    ladderList.push_back(ladder);
                    break;
                }
                case BLOCKTYPE_BLOCK_LADDER_ON_DIRT: {
                    Ladder ladder = Ladder(x, y, true);
                    ladderList.push_back(ladder);
                    break;
                }
                case BLOCKTYPE_EXITPOINT: {
                    ExitPoint exitPoint = ExitPoint(x, y);
                    exitPointList.push_back(exitPoint);
                    break;
                }
                case BLOCKTYPE_ENEMY_DINO: {
                    EnemyDino enemyDino = EnemyDino(x, y);
                    enemyDinoList.push_back(enemyDino);
                    break;
                }
                case BLOCKTYPE_ENEMY_DINO_ON_GRASS: {
                    EnemyDino enemyDino = EnemyDino(x, y);
                    enemyDinoList.push_back(enemyDino);
                    Grass grass = Grass(x, y);
                    grassList.push_back(grass);
                    break;
                }
                case BLOCKTYPE_ENEMYCOLLIDER: {
                    EnemyCollider enemyCollider = EnemyCollider(x, y);
                    enemyColliderList.push_back(enemyCollider);
                    break;
                }
                case BLOCKTYPE_ENEMYCOLLIDER_ON_GRASS: {
                    EnemyCollider enemyCollider = EnemyCollider(x, y);
                    enemyColliderList.push_back(enemyCollider);
                    Grass grass = Grass(x, y);
                    grassList.push_back(grass);
                    break;
                }
            }
        }
    }
    return true;
}


int main(int argc, char* argv[]) {
	bool quit = false;
	SDL_Event e;
    
    printf(
        "This program is free software: you can redistribute it and/or modify\n"
        "it under the terms of the GNU General Public License as published by\n"
        "the Free Software Foundation, either version 3 of the License, or\n"
        "(at your option) any later version.\n\n"
        "This program is distributed in the hope that it will be useful,\n"
        "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
        "GNU General Public License for more details.\n\n"
        "You should have received a copy of the GNU General Public License\n"
        "along with this program.  If not, see <http://www.gnu.org/licenses/>.\n\n"
    );
	
	if (!init()) {
        return 1;
    }
    if (!loadAllFiles()) {
        return 1;
    }
    
    Uint8 noOfLevels = 0;
    
    for (directory_iterator it("levels"); it != directory_iterator(); ++it) {
        noOfLevels++;
        if (!loadLevel(noOfLevels)) {
            return 1;
        }
        else {
            vector<Sprite*> spritesList;
            Uint32 lastFPS = SDL_GetTicks();
            Uint32 currentFPS;
            Uint32 framesSinceLastCheck;
            int windowHeight, windowWidth;
            while (!quit) {
                // Calculate camera offsets
                SDL_GetWindowSize(window, &windowWidth, &windowHeight);
                
                if ((player.getCentreX() > windowWidth/2) && (levelWidth > windowWidth)) {
                    // More than halfway across the window
                    cameraOffsetX = windowWidth/2 - player.getCentreX();
                }
                else if ((player.getCentreX() < windowWidth/2) && (levelWidth > windowWidth)) {
                    // Less than halfway across the screen
                    cameraOffsetX = 0;
                }
                
                if ((player.getCentreX() > (levelWidth-windowWidth/2))  && (levelWidth > windowWidth)) {
                    // Closer to the edge of the level than half a window
                    cameraOffsetX = -(levelWidth-windowWidth);
                }
                
                // Lock player to the centre of the window along the Y-axis
                cameraOffsetY = windowHeight/2 - player.getCentreY();
                
                // Clear screen with sky blue
                SDL_SetRenderDrawColor(renderer, 0xB7, 0xE8, 0xE5, 255);
                SDL_RenderClear(renderer);
                
                // Add blocks to sprites list
                for (auto &dirt : dirtList) {
                    spritesList.push_back(&dirt);
                }
                
                for (auto &grass : grassList) {
                    spritesList.push_back(&grass);
                }
                
                for (auto &ladder : ladderList) {
                    spritesList.push_back(&ladder);
                }
                
                for (auto &exitPoint : exitPointList) {
                    spritesList.push_back(&exitPoint);
                }
                
                for (auto &enemyDino : enemyDinoList) {
                    spritesList.push_back(&enemyDino);
                }
                
                // Loop through events
                while (SDL_PollEvent(&e) != 0) {
                    if (e.type == SDL_QUIT) {
                        quit = true;
                        printf("Quitting\n");
                    }
                    
                    if (e.type == SDL_WINDOWEVENT) {
                        switch (e.window.event) {
                            case SDL_WINDOWEVENT_RESIZED: {
                                // Recalculate offset so that the level is centred within the window
                                SDL_GetWindowSize(window, &windowWidth, &windowHeight);
                                
                                if (levelHeight < windowHeight) {
                                    centreOffsetY = (windowHeight-levelHeight)/2;
                                }
                                else if (levelHeight > windowHeight) {
                                    centreOffsetY = -((levelHeight-windowHeight)/2);
                                }
                                else {
                                    centreOffsetY = 0;
                                }
                                
                                if (levelWidth < windowWidth) {
                                    centreOffsetX = (windowWidth-levelWidth)/2;
                                }
                                else {
                                    centreOffsetX = 0; // We don't want to centre to the middle of the level along the X-axis as the camera will do that for us as the player moves
                                }
                                
                                //printf("windowHeight: %i\tlevelHeight: %i\tCentre Offset Y: %f\n", windowHeight, levelHeight, centreOffsetY);
                                break;
                            }
                        }
                    }
                }
                
                // Handle key presses
                const Uint8* keys = SDL_GetKeyboardState(NULL);
                
                // Left key pressed
                if (keys[SDL_SCANCODE_A]) {
                    player.setDir(Player::Direction::Left);
                    if (!player.onLadder) {
                        player.modPosX(-6);
                    }
                    for (auto &dirt : dirtList) {
                        if (player.collidedWith(&dirt) && player.getVelocityY() >= 0 && player.getBottomY() <= (&dirt)->getBottomY()) {
                            player.setPosX((&dirt)->getRightX());
                        }
                    }
                }
                
                // Right key pressed
                if (keys[SDL_SCANCODE_D]) {
                    player.setDir(Player::Direction::Right);
                    if (!player.onLadder) {
                        player.modPosX(6);
                    }
                    for (auto &dirt : dirtList) {
                        if (player.collidedWith(&dirt) && player.getVelocityY() >= 0 && player.getBottomY() <= (&dirt)->getBottomY()) {
                            player.setPosX((&dirt)->getPosX()-player.getWidth());
                        }
                    }
                }
                
                // Up key pressed
                if (keys[SDL_SCANCODE_W]) {
                    if (player.collidedWithAny(ladderList) && !player.jumping) {
                        player.onLadder = true;
                        player.modPosY(-6);
                        for (auto &dirt : dirtList) {
                            if (player.collidedWith(&dirt)) {
                                player.setPosY((&dirt)->getBottomY());
                            }
                        }
                        
                        for (auto &ladder : ladderList) {
                            if (player.collidedWith(&ladder)) {
                                if (player.getCentreX() < (&ladder)->getCentreX()) {
                                    player.modPosX(3);
                                }
                                else if (player.getCentreX() > (&ladder)->getCentreX()) {
                                    player.modPosX(-3);
                                }
                            }
                        }
                    }
                    else {
                        player.onLadder = false;
                    }
                }
                
                // Down key pressed
                if (keys[SDL_SCANCODE_S]) {
                    player.modPosY(6);
                    if (player.collidedWithAny(ladderList)) {
                        player.onLadder = true;
                    }
                    for (auto &dirt : dirtList) {
                        if (player.collidedWith(&dirt)) {
                            player.setPosY((&dirt)->getPosY()-player.getHeight());
                            player.onLadder = false;
                        }
                    }
                    if (player.onLadder && !player.jumping) {
                        for (auto &dirt : dirtList) {
                            if (player.collidedWith(&dirt)) {
                                player.setPosY((&dirt)->getPosY()-player.getHeight());
                                player.onLadder = false;
                            }
                        }
                    
                        for (auto &ladder : ladderList) {
                            if (player.collidedWith(&ladder)) {
                                if (player.getCentreX() < (&ladder)->getCentreX()) {
                                    player.modPosX(1);
                                }
                                else if (player.getCentreX() > (&ladder)->getCentreX()) {
                                    player.modPosX(-1);
                                }
                            }
                        }
                    }
                }
                
                // Space key pressed
                if (keys[SDL_SCANCODE_SPACE]) {
                    // If not jumping and not on ladder, jump
                    if (!player.jumping && !player.onLadder) {
                        player.jumping = true;
                        player.setVelocityY(0.1);
                    }
                }
                
                if (player.jumping) {
                    // Roll off velocity due to gravity
                    player.modVelocityY(-0.005);
                    
                    for (auto &dirt : dirtList) {
                        if (player.collidedWith(&dirt)) {
                            // If player falling and lands on dirt
                            if ((player.getVelocityY() <= 0) && (player.getBottomY() > (&dirt)->getPosY())) {
                                player.jumping = false;
                                player.setPosY((&dirt)->getPosY()-player.getHeight());
                                player.setVelocityY(0);
                            }
                            // If player going up and hits dirt
                            if ((player.getVelocityY() > 0) && (player.getPosY() < (&dirt)->getBottomY())) {
                                player.setPosY((&dirt)->getBottomY());
                                player.setVelocityY(0);
                            }
                        }
                    }
                    
                    for (auto &ladder : ladderList) {
                        if (player.collidedWith(&ladder)) {
                            // If player falling and lands on ladder on dirt
                            if ((player.getVelocityY() <= 0) && (player.getBottomY() > (&ladder)->getPosY()) && (&ladder)->onDirt) {
                                player.jumping = false;
                                player.setPosY((&ladder)->getPosY()-player.getHeight());
                                player.setVelocityY(0);
                            }
                            // If player going up and hits ladder on dirt
                            if ((player.getVelocityY() > 0) && (player.getPosY() < (&ladder)->getBottomY())&& (&ladder)->onDirt) {
                                player.setPosY((&ladder)->getBottomY());
                                player.setVelocityY(0);
                            }
                        }
                    }
                }
                
                // Not on ladder, not colliding with dirt or ladder, and not jumping, so fall
                if (!player.onLadder && !player.jumping && !player.collidedWithAny(dirtList)) {
                    player.modPosY(8);
                    
                    if (player.collidedWithAny(ladderList) && !player.collidedWithAny(dirtList)) {
                        player.jumping = false;
                        for (auto &ladder : ladderList) {
                            if (player.collidedWith(&ladder) && (&ladder)->onDirt) {
                                if (player.getBottomY() > (&ladder)->getPosY()) {
                                    player.setPosY((&ladder)->getPosY()-player.getHeight());
                                }
                            }
                        }
                    }
                    
                    if (player.collidedWithAny(dirtList)) {
                        player.jumping = false;
                        for (auto &dirt : dirtList) {
                            if (player.collidedWith(&dirt)) {
                                if (player.getBottomY() > (&dirt)->getPosY()) {
                                    player.setPosY((&dirt)->getPosY()-player.getHeight());
                                }
                            }
                        }
                    }
                }
                
                // Send the player back to the spawn point if they collide with an enemy
                if (player.collidedWithAny(enemyDinoList)) {
                    player.setPosX(spawnX);
                    player.setPosY(spawnY-player.getHeight()+BLOCK_SIZE);
                }
                
                // Send the player to the next level if they collide with an exit point
                if (player.collidedWithAny(exitPointList)) {
                    break;
                }
                
                // Update enemy positions: if collided with dirt or collider, go the other way
                for (auto &enemyDino : enemyDinoList) {
                    if ((&enemyDino)->getFlip() == SDL_FLIP_NONE) {
                        (&enemyDino)->modPosX(3);
                        if ((&enemyDino)->collidedWithAny(dirtList) || (&enemyDino)->collidedWithAny(enemyColliderList)) {
                            (&enemyDino)->setFlip(SDL_FLIP_HORIZONTAL);
                        }
                    }
                    else {
                        (&enemyDino)->modPosX(-3);
                        if ((&enemyDino)->collidedWithAny(dirtList) || (&enemyDino)->collidedWithAny(enemyColliderList)) {
                            (&enemyDino)->setFlip(SDL_FLIP_NONE);
                        }
                    }
                }
                
                // Iterate through sprites list and update each sprite in z-index order
                for (int i = BLOCK_Z_MIN; i < BLOCK_Z_MAX+1; i++) {
                    for (auto spritesIt = spritesList.begin(); spritesIt != spritesList.end(); ++spritesIt) {
                        if ((*spritesIt)->getPosZ() == i) {
                            (*spritesIt)->update();
                        }
                        if (i == BLOCK_Z_PLAYER) {
                            player.update();
                        }
                    }
                }
                
                // Update window
                SDL_RenderPresent(renderer);
                
                // Clear temporary sprites list
                spritesList.clear();
                
                framesSinceLastCheck++;
                if (lastFPS < SDL_GetTicks() - 1000) {
                    lastFPS = SDL_GetTicks();
                    currentFPS = framesSinceLastCheck;
                    framesSinceLastCheck = 0;
                }
                
                //printf("%i\n", currentFPS);
            }
        }
    }
	close();
	return 0;
}
