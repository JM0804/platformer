/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "Sprite.h"
#pragma once

class EnemyDino : public Sprite {
    public:
        EnemyDino(float xPos, float yPos);
        void update();
};

EnemyDino::EnemyDino(float xPos, float yPos) : Sprite(xPos, yPos) {
    image = IMAGE_DINO;
    w = BLOCK_SIZE;
    x = xPos*w;
    h = BLOCK_SIZE;
    y = yPos*h;
    z = BLOCK_Z_DIRT;
    angle = 0;
    flip = SDL_FLIP_NONE;
}

void EnemyDino::update() {
    if (x < 0) {
        x = 0;
        if (flip == SDL_FLIP_HORIZONTAL) {
            flip = SDL_FLIP_NONE;
        }
    }
    else if (x+w > levelWidth) {
        x = levelWidth-w;
        if (flip == SDL_FLIP_NONE) {
            flip = SDL_FLIP_HORIZONTAL;
        }
    }
    
    SDL_Rect rect = {
        (int)(x + centreOffsetX + cameraOffsetX),
        (int)(y + centreOffsetY + cameraOffsetY),
        (int)w,
        (int)h
    };
    
    SDL_RenderCopyEx(renderer, image, NULL, &rect, 0, NULL, flip);
}
