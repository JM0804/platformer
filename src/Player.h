/*  Copyright (C) 2017 Jonathan Mason <jm0804 AT protonmail DOT ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#pragma once

#include "Sprite.h"

using namespace std;

class Player {
    public:
        Sprite sprite = Sprite(0, 0);
        enum Direction {
            Left,
            Right
        };
        Player(float xPos, float yPos);
        virtual void update();
        virtual bool collidedWith(Sprite* otherSprite);
        bool collidedWithAny(vector<auto>& spriteList);
        
        // Getters
        Direction getDir();
        SDL_Rect* getRect();
        float getPosX();
        float getPosY();
        int getPosZ();
        float getHeight();
        float getWidth();
        float getVelocityX();
        float getVelocityY();
        float getCentreX();
        float getCentreY();
        float getBottomY();
        float getRightX();
        SDL_Texture* getImage();
        int getAngle();
        SDL_RendererFlip getFlip();
        
        // Setters
        void setDir(Direction dir);
        void setRect(SDL_Rect newRect);
        void setPos(float newX, float newY);
        void setPosX(float newX);
        void setPosY(float newY);
        void setPosZ(int newZ);
        void setHeight(float newHeight);
        void setWidth(float newWidth);
        void setVelocityX(float newVelocityX);
        void setVelocityY(float newVelocityY);
        void setImage(SDL_Texture* newImage);
        void setAngle(int newAngle);
        void setFlip(SDL_RendererFlip newFlip);
        
        // Modders
        void modVelocityX(float xMod);
        void modVelocityY(float yMod);
        void modPosX(float xMod);
        void modPosY(float yMod);
        
        bool onLadder = false;
        bool jumping = false;
    private:
        int facing = Direction::Right;
};

Player::Player(float xPos, float yPos) {
    sprite.setRect({
        (int)(xPos*BLOCK_SIZE),
        (int)((yPos*BLOCK_SIZE)-(0.5*BLOCK_SIZE)),
        (int)(BLOCK_SIZE*0.8),
        (int)(BLOCK_SIZE*1.5)
    });
    sprite.setPosZ(BLOCK_Z_PLAYER);
    sprite.setImage(IMAGE_PLAYER);
}

void Player::setDir(Direction dir) {
    if (dir == Direction::Left) {
        sprite.setFlip(SDL_FLIP_HORIZONTAL);
    }
    else {
        sprite.setFlip(SDL_FLIP_NONE);
    }
}

void Player::update() {
    if (sprite.getPosX() < 0) {
        sprite.setPosX(0);
    }
    else if (sprite.getPosX()+sprite.getWidth() > levelWidth) {
        sprite.setPosX(levelWidth-sprite.getWidth());
    }
    
    sprite.setPosY(sprite.getPosY() - sprite.getVelocityY());
    
    SDL_Rect rect = {
        (int)(sprite.getPosX() + centreOffsetX + cameraOffsetX),
        (int)(sprite.getPosY() + centreOffsetY + cameraOffsetY),
        (int)sprite.getWidth(),
        (int)sprite.getHeight()
    };
    
    SDL_RenderCopyEx(renderer, sprite.getImage(), NULL, &rect, 0, NULL, sprite.getFlip());
}

bool Player::collidedWith(Sprite* otherSprite) {
    return sprite.collidedWith(otherSprite);
}

bool Player::collidedWithAny(vector<auto>& spriteList) {
    sprite.collidedWithAny(spriteList);
}

// Getters
Player::Direction Player::getDir() {
    SDL_RendererFlip dir = sprite.getFlip();
    
    if (dir == SDL_FLIP_NONE) {
        return Player::Direction::Right;
    }
    else {
        return Player::Direction::Left;
    }
}

SDL_Rect* Player::getRect() {
    return sprite.getRect();
}

float Player::getPosX() {
    return sprite.getPosX();
}

float Player::getPosY() {
    return sprite.getPosY();
}

int Player::getPosZ() {
    return sprite.getPosZ();
}

float Player::getHeight() {
    return sprite.getHeight();
}

float Player::getWidth() {
    return sprite.getWidth();
}

float Player::getVelocityY() {
    return sprite.getVelocityY();
}

float Player::getCentreX() {
    sprite.getCentreX();
}

float Player::getCentreY() {
    sprite.getCentreY();
}

float Player::getBottomY() {
    sprite.getBottomY();
}

float Player::getRightX() {
    sprite.getRightX();
}

SDL_Texture* Player::getImage() {
    return sprite.getImage();
}

int Player::getAngle() {
    return sprite.getAngle();
}

SDL_RendererFlip Player::getFlip() {
    return sprite.getFlip();
}

// Setters
void Player::setRect(SDL_Rect newRect) {
    sprite.setRect(newRect);
}

void Player::setPos(float newX, float newY) {
    sprite.setPos(newX*BLOCK_SIZE, (newY*BLOCK_SIZE)-(0.5*BLOCK_SIZE));
}

void Player::setPosX(float newX) {
    sprite.setPosX(newX);
}

void Player::setPosY(float newY) {
    sprite.setPosY(newY);
}

void Player::setPosZ(int newZ) {
    sprite.setPosZ(newZ);
}

void Player::setHeight(float newH) {
    sprite.setHeight(newH);
}

void Player::setWidth(float newW) {
    sprite.setWidth(newW);
}

void Player::setVelocityY(float newVelocityY) {
    sprite.setVelocityY(newVelocityY);
}

void Player::setImage(SDL_Texture* newImage) {
    sprite.setImage(newImage);
}

void Player::setAngle(int newAngle) {
    sprite.setAngle(newAngle);
}

void Player::setFlip(SDL_RendererFlip newFlip) {
    sprite.setFlip(newFlip);
}

// Modders
void Player::modVelocityY(float yMod) {
    sprite.modVelocityY(yMod);
}

void Player::modPosX(float xMod) {
    sprite.modPosX(xMod);
}

void Player::modPosY(float yMod) {
    sprite.modPosY(yMod);
}
